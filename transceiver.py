from est_sync import EstSync
from channel_est_sync.utils import save_wav
from week1_challenge.will_fns import Constellation, getArgand, getBytes
from standards import *
from audio_driver import playAudio, recordAudio, playRecAudio

import numpy as np
from scipy import signal
from scipy import io
from scipy.stats import norm
import matplotlib.pyplot as plt
from ldpc_jossy.py.ldpc import code
import os

DO_LDPC = True
SKIP_LAST_KNOWN = False


def estKnownSymbols(startSymbolIn, endSymbolIn, offsetPerSymbol, numSymbols, plot=False):
    startSymbol = startSymbolIn.copy()
    endSymbol = endSymbolIn.copy()
    # Reduce offsetPerSymbol as no prefixes
    offsetPerKnownSymbol = (FFT_LEN/(FFT_LEN+PREFIX_LEN))*offsetPerSymbol
    offsetExpsKnown = np.array([np.exp(1j*2*np.pi*n*-offsetPerKnownSymbol/FFT_LEN) for n in range(FFT_LEN)])
    # Get symbols
    symbols = []
    startSymbol = startSymbol[PREFIX_LEN:]
    print(len(startSymbol))
    for i in range(KNOWN_SYMBOLS):
        symbol_t = startSymbol[:FFT_LEN]
        startSymbol = startSymbol[FFT_LEN:]
        symbol = np.fft.fft(symbol_t, FFT_LEN)
        symbols.append(symbol)
    endSymbol = endSymbol[PREFIX_LEN:]
    for i in range(KNOWN_SYMBOLS):
        symbol_t = endSymbol[:FFT_LEN]
        endSymbol = endSymbol[FFT_LEN:]
        symbol = np.fft.fft(symbol_t, FFT_LEN)
        symbols.append(symbol)
    # for symbol in symbols:
    #     print(getArgand([symbol[100], symbol[200], symbol[300]]))
    lests = []
    lbig = None
    for i in range(KNOWN_SYMBOLS*2 - 1):
        start = DATA_START_INDEX
        stop = DATA_STOP_INDEX
        s1 = symbols[i][start:stop]
        s2 = symbols[i+1][start:stop]
        n = np.arange(len(s1))
        fn = np.imag(np.log(s2/s1))
        poly = np.polyfit(n, fn, 1)
        print(poly)
        l = poly[0]*FFT_LEN/(2*np.pi)
        print(l)
        if i == KNOWN_SYMBOLS-1:
            lbig = (l/(numSymbols - KNOWN_SYMBOLS*2))*(FFT_LEN/(FFT_LEN+PREFIX_LEN))
        else:
            lests.append(l)
        if plot:
            plt.plot(n, fn)
            plt.plot(n, n*poly[0] + poly[1])
            plt.title(f"Offsets from symbol {i} to {i+1}")
            plt.pause(1.0)
            plt.close()
    print("lests", lests, "lbig", lbig)
    newOffsetPerKnownSymbol = -np.mean(lests)
    # newOffsetPerKnownSymbol = lbig
    print("New offset", newOffsetPerKnownSymbol, "Old offset", offsetPerKnownSymbol)
    print("New total offset", newOffsetPerKnownSymbol*numSymbols/(FFT_LEN/(FFT_LEN+PREFIX_LEN)), "Old total offset", offsetPerKnownSymbol*numSymbols/(FFT_LEN/(FFT_LEN+PREFIX_LEN)))
    useNew = input("Use new? (y for yes, anything else for no)")
    if useNew == 'y':
        offsetPerKnownSymbol = newOffsetPerKnownSymbol
    offsetExpsKnown = np.array([np.exp(1j*2*np.pi*n*-offsetPerKnownSymbol/FFT_LEN) for n in range(FFT_LEN)])
    offsetPerSymbol = offsetPerKnownSymbol/(FFT_LEN/(FFT_LEN+PREFIX_LEN))
    offsetExps = np.array([np.exp(1j*2*np.pi*n*-offsetPerSymbol/FFT_LEN) for n in range(FFT_LEN)])
    # Now divide by offset exps
    for i in range(KNOWN_SYMBOLS*2):
        symbols[i] /= offsetExpsKnown**(i)
    avStart = np.mean(symbols[:KNOWN_SYMBOLS-SKIP_LAST_KNOWN], axis=0)
    avEnd = np.mean(symbols[KNOWN_SYMBOLS:], axis=0)
    avEnd /= offsetExps**(numSymbols - KNOWN_SYMBOLS)
    # print(getArgand([avStart[100], avStart[200], avStart[300]]), "Start symbols")
    # print(getArgand([avEnd[100], avEnd[200], avEnd[300]]), "End symbols")
    knownSymbol = Transmitter.getKnownSymbol()
    channel_f = avStart/knownSymbol
    channel_f_end = avEnd/knownSymbol
    if plot:
        plt.plot(np.angle(channel_f), label="Start response")
        plt.plot(np.angle(channel_f_end), label="End response")
        plt.legend()
        plt.show()
    # channel_f = (channel_f + channel_f_end)/2
    return channel_f, offsetExps


def encodeLDPC(txData):
    c = code(CODE_STANDARD, CODE_RATE, CODE_Z, CODE_PTYPE)
    print("N, K", c.N, c.K)
    txDataBits = []
    for byte in txData:
        for i in range(7, -1, -1):
            txDataBits.append(byte >> i & 0b1)
    # print(txData)
    # print([f"{txData[i]:08b}" for i in range(10)])
    # print(txDataBits[:80])
    # print(len(txData))
    packets = [txDataBits[i:i+c.K] for i in range(0, len(txDataBits), c.K)]
    # Pad final packet with random data
    while len(packets[-1]) < c.K:
        packets[-1].append(np.random.randint(2))
    encPackets = []
    for packet in packets:
        encPackets.append(c.encode(packet))
        # print("Sent", packet[100:110])
    encBits = np.concatenate(encPackets)
    bitPairs = []
    for i in range(0, len(encBits), 2):
        bitPair = (encBits[i] << 1) | encBits[i+1]
        bitPairs.append(bitPair)
    return bitPairs


def decodeLDPC(llrs):
    c = code(CODE_STANDARD, CODE_RATE, CODE_Z, CODE_PTYPE)
    packets = [np.array(llrs[i:i+c.N]) for i in range(0, len(llrs), c.N)]
    if len(packets[-1]) < c.N:
        # Discard last packet, as must just be padding
        packets = packets[:-1]
    decodedBits = []
    for packet in packets:
        app, it = c.decode(packet)
        x = (app < 0).astype(int)
        bits = x[:c.K]
        decodedBits.extend(bits)
        print(f"LDPCDecoded {len(decodedBits)//8} bytes out of {len(llrs)//16}    ", end="\r")
        # print("Received", bits[100:110])
    print("\nDone")
    decodedBytes = []
    for i in range(0, len(decodedBits), 8):
        byte = 0
        try:
            for j in range(8):
                byte |= decodedBits[i+j] << (7-j)
            # print(decodedBits[i:i+8])
            # print(f"{byte:08b}")
            decodedBytes.append(byte)
        except IndexError:
            print("Out of bytes to decode")
            break
    return decodedBytes


class Transmitter:
    def __init__(self, n=4, cw=False):
        self.constellation = Constellation(n, cw)
        self.n = n
        if n != 4:
            raise NotImplementedError("OFDM symbols other than n=4 have not been implemented")

    def transmitFileWithSimChannel(self, filename, channel, saveWav=False):
        data = self.transmitFromFile(filename, saveWav=saveWav)
        data = np.convolve(data, channel)
        if saveWav:
            save_wav(data, 'data/dataChanneled.wav', **{'sample_rate': SAMPLING_FREQ})
        else:
            playAudio(data)
        return data
    
    def transmitDataWithSimChannel(self, data, channel, saveWav=False):
        data = self.transmitData(data, saveWav=saveWav)
        data = np.convolve(data, channel)
        if saveWav:
            save_wav(data, 'data/dataChanneled.wav', **{'sample_rate': SAMPLING_FREQ})
        else:
            playAudio(data)
        return data

    def transmitFromFile(self, filename, saveWav=False, txFileName=None):
        data, txData = self.getTransmitDataFromFile(filename, txFileName=txFileName)
        for val in data:
            assert val == np.real(val)
        data = np.real(data)
        np.save("data/txData.npy", data)
        if saveWav:
            save_wav(data, 'data/data.wav', **{'sample_rate': SAMPLING_FREQ})
        else:
            playAudio(data)
        return data, txData

    def transmitData(self, txData, saveWav=False):
        if DO_LDPC:
            txBitPairs = encodeLDPC(txData)
            data = self.getTransmitData(txBitPairs, isBitPairs=True)
        else:
            data = self.getTransmitData(txData)
        for val in data:
            assert val == np.real(val)
        data = np.real(data)
        if saveWav:
            save_wav(data, 'data/data.wav', **{'sample_rate': SAMPLING_FREQ})
        else:
            playAudio(data)
        return data
    
    def getTransmitDataFromFile(self, filename, txFileName=None):
        f = open(f"{filename}", 'rb')
        txData = f.read()
        f.close()
        if txFileName is None:
            nameBytes = bytes(filename, 'ascii')
        else:
            nameBytes = bytes(txFileName, 'ascii')
        sizeBytes = []
        for i in range(4):
            sizeBytes.append((len(txData) >> 8*(3-i)) & 0xff)
        # For little endian
        sizeBytes.reverse()
        sizeBytes = bytes(sizeBytes)
        print("Size", len(txData), sizeBytes)
        txData = sizeBytes + nameBytes + bytes('\0', 'ascii') + txData
        if DO_LDPC:
            txBitPairs = encodeLDPC(txData)
            return self.getTransmitData(txBitPairs, isBitPairs=True), txData
        else:
            return self.getTransmitData(txData), txData

    def getChirp(self=None):
        # TODO: Try windowing the chirps (cosine rolloff suggested) to prevent ringing
        t = np.linspace(0, CHIRP_LENGTH, CHIRP_LENGTH*SAMPLING_FREQ)
        w = signal.chirp(t, f0=CHIRP_START_FREQ, f1=CHIRP_STOP_FREQ, t1=CHIRP_LENGTH, method='linear')
        # window = signal.windows.cosine(CHIRP_LENGTH*SAMPLING_FREQ)
        # return w*window
        return w

    def getSyncPrefix(self):
        w = self.getChirp()
        z = np.zeros(PRE_TRANSMISSION_SILENCE*SAMPLING_FREQ)
        w = np.concatenate((z, w))
        return w

    def getSyncSuffix(self):
        w = self.getChirp()
        z = np.zeros(PRE_TRANSMISSION_SILENCE*SAMPLING_FREQ)
        w = np.concatenate((w, z))
        return w

    def getKnownSymbol(self=None):
        packet_len = FFT_LEN//2 - 1
        known_symbol = np.load("GF3_known_ofdm_block/known_ofdm_symbol.npy")
        assert len(known_symbol) == packet_len*2 + 2
        return known_symbol

    def getKnownxFix(self=None):
        if self is None:
            self = Transmitter()
        known_symbol = self.getKnownSymbol()
        kt = np.fft.ifft(known_symbol, FFT_LEN)
        kcp = kt[-PREFIX_LEN:]
        known_repeat = np.tile(kt, KNOWN_SYMBOLS)
        known_xfix = np.concatenate((kcp, known_repeat))
        return np.real(known_xfix)

    def getTransmitData(self, data: list[int], isBitPairs=False):
        if not isBitPairs:
            bitPairs = []
            for i in range(len(data)):
                byte = data[i] & 0xff
                bits0 = byte >> 6 & 0b11
                bits1 = byte >> 4 & 0b11
                bits2 = byte >> 2 & 0b11
                bits3 = byte >> 0 & 0b11
                bitPairs.extend([bits0, bits1, bits2, bits3])
        else:
            bitPairs = data
        packet_len = FFT_LEN//2 - 1
        # TODO: set packet len based on useable frequency range
        packet_data_len = DATA_STOP_INDEX-DATA_START_INDEX
        packets = [bitPairs[i:i+packet_data_len] for i in range(0, len(bitPairs), packet_data_len)]
        # Pad final packet with random data
        while len(packets[-1]) < packet_data_len:
            packets[-1].append(np.random.randint(4))
        symbols = []
        for packet in packets:
            # print("Packet len", len(packet))
            paddedPacket = np.random.randint(0, 4, size=packet_len)
            for i in range(len(packet)):
                paddedPacket[i+DATA_START_INDEX-1] = packet[i]
            dataSymbol = np.zeros(packet_len, dtype=np.complex_)
            for i in range(len(paddedPacket)):
                dataSymbol[i] = self.constellation.encode(paddedPacket[i])
            symbol = np.concatenate((np.zeros(1), dataSymbol, np.zeros(1), np.flip(np.conjugate(dataSymbol))))
            # print(getArgand([symbol[DATA_START_INDEX]]), packet[0])
            # print(getArgand([symbol[DATA_STOP_INDEX-1]]), packet[-1])
            # print(self.constellation)
            # input()
            # print(symbol[0], symbol[FFT_LEN//2], "Should be real 0s")
            for i in range(1, 10):
                assert symbol[i] == np.conjugate(symbol[-i])
            # print(symbol[1], symbol[-1], "Should be complex conjugates")
            symbols.append(symbol)
        
        # Now take iffts
        for i in range(len(symbols)):
            symbols[i] = np.fft.ifft(symbols[i], FFT_LEN)
        # Just to prove they're all real
        for symbol in symbols:
            for i in symbol:
                assert i == np.real(i)
        # Now add prefixes
        for i in range(len(symbols)):
            symbol = symbols[i]
            prefix = symbol[-PREFIX_LEN:]
            symbols[i] = np.concatenate((prefix, symbol))

        frames = [symbols[i:i+FRAME_LENGTH] for i in range(0, len(symbols), FRAME_LENGTH)]
        
        print("Num symbols sending", len(symbols))
        print("Num frames", len(frames))

        retVal = []
        for frame in frames:
            ret = np.concatenate(frame)
            # Add known symbols
            ret = np.concatenate((self.getKnownxFix(), ret, self.getKnownxFix()))
            # Make real
            ret = np.real(ret)
            # Now normalise between -1 and 1 for max volume
            ret /= abs(max(ret, key=abs))
            # Add chirps
            ret = np.concatenate((self.getChirp(), ret, self.getChirp()))
            retVal.append(ret)
        retVal = np.concatenate(retVal)
        # Add sync prefix and suffix
        retVal = np.concatenate((
            self.getSyncPrefix(),
            retVal,
            self.getSyncSuffix()))
        return retVal


class Receiver:
    def __init__(self, n=4, cw=False):
        self.constellation = Constellation(n, cw)
        self.n = n
        if n != 4:
            raise NotImplementedError("OFDM symbols other than n=4 have not been implemented")

    def receiveToFile(self, inFileName=None, duration=None):
        if inFileName is not None:
            samplerate, data = io.wavfile.read(inFileName)
            assert samplerate == SAMPLING_FREQ
        elif duration is not None:
            data = recordAudio(duration)
        else:
            raise NotImplementedError("Cannot receive without duration or file")
        self.decode(data, toFile=True)

    def decode(self, rxData, toFile=True, plot=False):
        chirp = Transmitter.getChirp()
        est = EstSync(chirp, rxData, len(chirp))
        est.synchronize(method='conv_all', plot=plot)

        all_decoded_data = []
        allDataPackets = []
        numSymbols = 0
        for frame in est.frames:
            # Get start and end of data
            start = frame[0]
            end = frame[1]
            print("est start and end", start, end)
            print(f"diff should be divisible by {FFT_LEN+PREFIX_LEN} if in sync")
            missingKnownPrefixes = (KNOWN_SYMBOLS-1)*2*PREFIX_LEN
            diffDiv = (end-start + missingKnownPrefixes)/(FFT_LEN+PREFIX_LEN)
            offset = (end-start + missingKnownPrefixes) - round(diffDiv)*(FFT_LEN+PREFIX_LEN)
            offsetPerSymbol = offset/round(diffDiv)
            print(diffDiv, "is an offset of", offset, "per symbol", offsetPerSymbol)
            if plot:
                plt.plot(rxData, label="Orig")
                plt.plot(rxData[start:end], label="Cut")
                plt.show()
            res = self.decodeKnownChannel(
                rxData[start:end],
                offsetPerSymbol=offsetPerSymbol,
                numSymbols=round(diffDiv),
                plot=plot
            )
            all_decoded_data.extend(res[0])
            allDataPackets.extend(res[1])
            numSymbols += round(diffDiv)

        allDataExpectedLen = (numSymbols-KNOWN_SYMBOLS*2)*(DATA_STOP_INDEX-DATA_START_INDEX)

        if toFile:
            decodeHeaderBytes = all_decoded_data.copy()
            # Decode header
            fileSizeBytes = decodeHeaderBytes[:4]
            # For little endian
            fileSizeBytes.reverse()
            decodeHeaderBytes = decodeHeaderBytes[4:]
            filesize = 0
            filename = ""
            for i in range(4):
                filesize |= fileSizeBytes[i] << (8*(3-i))
            print("Filesize", filesize)
            while True:
                # Take next byte of data
                byte = decodeHeaderBytes.pop(0)
                # Decode char as best we can
                try:
                    char = bytes([byte]).decode('ascii')
                    assert byte >= 0x20 or byte == 0x00
                except (UnicodeDecodeError, AssertionError):
                    print(f"{byte:02x} is not in range, so make it in range")
                    byte &= 0x7f
                    if byte < 0x20 and byte != 0x00:
                        print(f"{byte:02x} is not printable, try oring with 0x20")
                        byte |= 0x20
                    char = bytes([byte]).decode('ascii')
                if char == '\0':
                    break
                # File name
                filename += char
            # print(f"Diffs, {int(bytes(filename, encoding='ascii').hex(), base=16) ^ int(bytes('files/9103751287.wav', encoding='ascii').hex(), base=16):b}")
            print(f"Now try decoding {filename} of length {filesize}")
            print(f"Expected filesize of about {(allDataExpectedLen//4)*CODE_RATE_FLT}")
            filesize = int(filesize)
            print(f"Now decoding {filename} of length {filesize}")
            thing = input("Continue?")
            if thing != '':
                print(thing)
                filesize = int(thing)
            # Save file
            data_bytes = decodeHeaderBytes[:filesize]
            os.makedirs(os.path.dirname(f"decoded/{filename}"), exist_ok=True)
            f = open(f"decoded/{filename}", 'wb')
            f.write(bytes(data_bytes))
            f.close()

        return all_decoded_data, allDataPackets

    def decodeKnownChannel(self, dataIn, channel=None, channel_f=None, offsetPerSymbol=None, numSymbols=None, doLDPC=True, plot=False):
        if offsetPerSymbol is not None:
            offsetExps = np.array([np.exp(1j*2*np.pi*n*-offsetPerSymbol/FFT_LEN) for n in range(FFT_LEN)])
        else:
            offsetExps = np.ones(FFT_LEN)
        file = dataIn.copy()
        if plot:
            plt.plot(file, label="file")
            plt.legend()
            plt.show()
        qpsk = self.constellation
        # FFT of channel for later
        if channel is not None:
            channel_f = np.fft.fft(channel, FFT_LEN)
        elif channel_f is None:
            channel_f = np.ones(FFT_LEN)
        elif len(channel_f) != FFT_LEN:
            channel = np.fft.ifft(channel_f)
            channel_f = np.fft.fft(channel, n=FFT_LEN)

        # TODO: Actually use known symbols for something, currently just discards
        knownLen = len(Transmitter.getKnownxFix())
        startKnownSymbols = file[:knownLen]
        endKnownSymbols = file[-knownLen:]
        channel_f, offsetExps = estKnownSymbols(startKnownSymbols, endKnownSymbols, offsetPerSymbol, numSymbols)
        # eg channel_f = estBasedOnSymbols(startKnownSymbols, endKnownSymbols, offset)
        file = file[knownLen:]
        file = file[:-knownLen]

        # TODO: Shift file back a few samples, to ensure you're always in the prefix (doesn't seem to be needed)
        shift = 10
        if offsetPerSymbol*numSymbols > shift:
            shift += round(offsetPerSymbol*numSymbols)
        flen = len(file)
        file = np.concatenate((np.zeros(shift), file))
        file = file[:flen]
        exps = np.array([np.exp(1j*2*np.pi*n*-shift/FFT_LEN) for n in range(FFT_LEN)])
        channel_f *= exps

        # Work out expected length of data based on num symbols
        allDataExpectedLen = (numSymbols-KNOWN_SYMBOLS*2)*(DATA_STOP_INDEX-DATA_START_INDEX)
        print("Calc expected len", allDataExpectedLen)
        print("Num symbols", numSymbols)

        num_blocks = 0
        all_data = []
        filesize = allDataExpectedLen//4
        allDataPackets = []
        allLLRs = []
        while len(all_data) < allDataExpectedLen:
            num_blocks += 1
            # Remove prefix and extract next block
            file = file[PREFIX_LEN:]
            block = file[:FFT_LEN]
            file = file[FFT_LEN:]
            # FFT and remove channel
            symbols = np.fft.fft(block, FFT_LEN)
            symbols /= channel_f
            # TODO: Now shift channel due to frequency mismatch for the next symbol
            channel_f *= offsetExps
            # TODO: Only take symbols from transmit region
            symbols = symbols[DATA_START_INDEX:DATA_STOP_INDEX]
            # Decode from symbols
            data = []
            llrs = []
            for symbol in symbols:
                data.append(qpsk.decode(symbol))
                llrs.extend(qpsk.getLLRs(symbol))
            all_data.extend(data.copy())
            allDataPackets.append(data.copy())
            allLLRs.extend(llrs.copy())
            print(f"Decoded {int(len(all_data)/4)} bytes out of {filesize}  ", end="\r")
        print("\nDone")

        all_data_bytes = []
        if doLDPC:
            all_data_bytes = decodeLDPC(allLLRs.copy())
        else:
            # Decode the 2bit pairs to bytes
            all_data_bytes, _ = getBytes(all_data.copy())

        return all_data_bytes, allDataPackets


if __name__ == "__main__":
    np.random.seed(500_400_200)
    ldpc = code()
    tx = Transmitter()
    rx = Receiver()
    # deviceChannel for playback from
    # other devcice and manual recording, or not for automated
    # recording and playback on same device (requires audio_driver.py edits)
    deviceChannel = True
    # useOldData will decode the data.wav file for data, rather than
    # generating random data and overwriting a new data.wav file
    useOldData = True
    fileName = "files/bee.ppm"
    # txFileName = "data/words.txt"
    txFileName = None
    # fileName = None
    toFile = True
    plot = False

    if fileName is not None:
        data, txData = tx.transmitFromFile(fileName, saveWav=True, txFileName=txFileName)
    else:
        if not useOldData:
            txData = np.random.randint(0, 256, size=10000, dtype=np.uint8)
            data = tx.transmitData(txData, saveWav=True)
            np.save("data/data.npy", txData)
        else:
            txData = np.load("data/data.npy")
            data = tx.transmitData(txData, saveWav=True)
    if deviceChannel:
        skip = input(f"Hit enter to record that at fs={SAMPLING_FREQ} (y to skip and use data/recorded.wav)")
        if skip != 'y':
            duration = len(data)/SAMPLING_FREQ + 5
            dataWav = recordAudio(duration)
            save_wav(dataWav, "data/recorded.wav", **{'sample_rate': SAMPLING_FREQ})
        else:
            print("Using recorded.wav")
            samplerate, dataWav = io.wavfile.read("data/recorded.wav")
        print("Recording complete")
    else:
        samplerate, dataWav = io.wavfile.read("data/data.wav")
        dataWav = playRecAudio(dataWav)
    dataWav = dataWav.flatten()
    rxData, rxPackets = rx.decode(dataWav, toFile=toFile, plot=plot)
    bitErrors = 0
    for i in range(len(txData)):
        mask = txData[i] ^ rxData[i]
        # print(f"{txData[i]:08b}, {rxData[i]:08b}")
        # print(f"{mask:08b}")
        for j in range(8):
            bitErrors += (mask >> j) & 0b1
        # print("Errors:", bitErrors)
        # print("BER", bitErrors/((i+1)*8))
        # input()
    print("Overall Bit Error Rate is", bitErrors/(len(txData)*8))

    # Binned BER analysis
    txBitPairs = []
    for i in range(len(txData)):
        byte = txData[i] & 0xff
        bits0 = byte >> 6 & 0b11
        bits1 = byte >> 4 & 0b11
        bits2 = byte >> 2 & 0b11
        bits3 = byte >> 0 & 0b11
        txBitPairs.extend([bits0, bits1, bits2, bits3])
    rxBitPairs = []
    for i in range(len(rxData)):
        byte = rxData[i] & 0xff
        bits0 = byte >> 6 & 0b11
        bits1 = byte >> 4 & 0b11
        bits2 = byte >> 2 & 0b11
        bits3 = byte >> 0 & 0b11
        rxBitPairs.extend([bits0, bits1, bits2, bits3])
    c = code(CODE_STANDARD, CODE_RATE, CODE_Z, CODE_PTYPE)
    packet_len = c.K//2
    txPackets = [txBitPairs[i:i+packet_len] for i in range(0, len(txBitPairs), packet_len)]
    rxPackets = [rxBitPairs[i:i+packet_len] for i in range(0, len(rxBitPairs), packet_len)]
    errs = np.zeros(packet_len)
    occs = np.zeros(packet_len)
    for i in range(len(txPackets)):
        txPacket = txPackets[i]
        rxPacket = rxPackets[i]
        for j in range(len(txPacket)):
            mask = txPacket[j] ^ rxPacket[j]
            errors = ((mask >> 1) & 0b1) + (mask & 0b1)
            errs[j] += errors
            occs[j] += 2
        BERs = errs/((i+1)*2)
        # Plot after each symbol, to see evolution over time
        plt.plot(BERs)
        plt.ylabel("Bit error rate")
        plt.title(f"Packet {i} of {len(txPackets)}")
        plt.pause(0.01)
        plt.close()
    BERs = errs/occs
    print("BER", np.mean(BERs))
    print("Final")
    input("Press enter to view")
    # Plot at end
    plt.plot(BERs)
    plt.ylabel("Bit error rate")
    plt.show()
