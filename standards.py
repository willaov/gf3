# Silence before transmission begins, to accomodate some computers making
# weird noises at the start of playback of files, in seconds
PRE_TRANSMISSION_SILENCE = 1

# Length of chirp in seconds, and frequencies for it to go between in Hz
CHIRP_LENGTH = 1
CHIRP_START_FREQ = 1_000
CHIRP_STOP_FREQ = 10_000

# Sampling frequency for audio, in Hz
SAMPLING_FREQ = 48_000

# FFT length
FFT_LEN = 4096

# Cyclic prefix length
PREFIX_LEN = 512

# Send known symbol every 5th symbol
KNOWN_SYMBOLS = 4

# Data start and stop frequency, and therefore index within OFDM
DATA_START_FREQ = 1_000
DATA_STOP_FREQ = 10_000
DATA_START_INDEX = 86
DATA_STOP_INDEX = 853 + 1   # +1 because standard is [] but we are [)

# Frame length
FRAME_LENGTH = 128

# LDPC
CODE_STANDARD = '802.16'
CODE_RATE='1/2'
CODE_RATE_FLT = 1./2.
CODE_Z = 64
CODE_PTYPE = 'A'

"""
    The transmission should consist of:
        Silence length PRE_TRANSMISSION_SILENCE
        Chirp
        Frames
        Chirp
        Silence length PRE_TRANSMISSION_SILENCE

    Each frame consists of
        Chirp
        Known OFDM symbols
        OFDM Data
        Known OFDM symbols
        Chirp
"""