import wave
import struct
from scipy.io import wavfile
import numpy as np

def save_wav(audio, file_name, **params):
    wavfile.write(file_name, params["sample_rate"], audio.astype(np.float32))

    # # Open up a wav file
    # wav_file=wave.open(file_name,"w")

    # # wav params
    # nchannels = 1
    # sampwidth = 2

    # nframes = len(audio)
    # comptype = "NONE"
    # compname = "not compressed"
    # wav_file.setparams((nchannels, sampwidth, params['sample_rate'], nframes, comptype, compname))

    # for sample in audio:
    #     wav_file.writeframes(struct.pack('h', int( sample * 32767.0 )))

    # wav_file.close()

    return