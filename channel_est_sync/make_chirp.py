from ctypes import util
from random import sample
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import signal as sig
import wave
import utils

sns.set()

sample_rate = 48000 #Hz
T = 10 #s
f0 = 0
f1 = 20000

t = np.linspace(0, T, T*sample_rate)
w = sig.chirp(t, f0=f0, f1=sample_rate/2, t1=T, method='linear')
print(len(w))
w = np.append(w, w)
print(len(w))
print(w)

# plt.plot(t, w)
# plt.show()

utils.save_wav(w, 'data/chirp_double.wav', **{'sample_rate': sample_rate})

