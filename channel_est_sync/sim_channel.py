from ctypes import util
from random import sample
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import signal as sig
import wave
from scipy.io import wavfile
import utils

sns.set()

# Simulated channel using the length 30 FIR given in weekend challenge

chirp = wavfile.read('data/chirp.wav')
chirp_data = chirp[1]
print(type(chirp_data))
print(np.shape(chirp_data))

# plt.plot(chirp_data[:10000])
# plt.show()

channel = np.ndarray.flatten(pd.read_csv('data/channel.csv', header=None).values)
print(type(channel))
print(np.shape(channel))

chirp_out = np.convolve(chirp_data, channel, mode='same')
print(np.shape(chirp_out))

plt.plot(chirp_out)
plt.show()

utils.save_wav(chirp_out, 'data/chirp_out.wav', **{'sample_rate':44100})



