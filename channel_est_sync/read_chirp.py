from ctypes import util
from random import sample
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import signal as sig
from scipy.io import wavfile
import wave
import utils

sns.set()

fs = 48000 #Hz

chirp = wavfile.read('data/chirp.wav')
chirp_data = chirp[1]
print(type(chirp_data))
print(np.shape(chirp_data))

# plt.plot(chirp_data)
# plt.show()

# channel = np.ndarray.flatten(pd.read_csv('data/channel.csv', header=None).values)
# print(type(channel))
# print(np.shape(channel))

chirp_out = wavfile.read('data/chirp_dpo.wav')
chirp_out = chirp_out[1]
print(np.shape(chirp_out))

# plt.plot(chirp_out)
# plt.show()

# utils.save_wav(chirp_out, 'data/chirp_out.wav', **{'sample_rate':44100})

match = np.convolve(chirp_out, chirp_data[::-1])
# plt.plot(match)

sig_end = np.argmax(match)
sig_start = sig_end - len(chirp_data)

print(f'Signal ends at: {np.argmax(match)}')

signal = chirp_out[sig_start:sig_end]

# plt.plot(signal)

freq = np.linspace(-fs/2, fs/2, len(signal))
channel = np.fft.fftshift(np.fft.fft(signal))/np.fft.fftshift(np.fft.fft(chirp_data))
# plt.plot(freq, abs(channel))
plt.plot(freq, np.arctan(channel.imag/channel.real))

plt.show()

