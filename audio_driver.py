import numpy as np
import sounddevice as sd
from standards import *
"""
    Edit these for your individual installation,
    but DO NOT COMMIT those changes
    Can run this file to get list of available devices
"""
outDev = 'MacBook Pro Speakers'
inDev = 'MacBook Pro Microphone'
sd.default.samplerate = SAMPLING_FREQ
sd.default.channels = 1
sd.default.device = [inDev, outDev]


def recordAudio(duration):
    recording = sd.rec(
        int(duration*SAMPLING_FREQ), blocking=True
    )
    return recording.astype(np.float32)


def playAudio(audio):
    sd.play(audio, blocking=True)


def playRecAudio(audio):
    recording = sd.playrec(audio, blocking=True)
    return recording.astype(np.float32)


if __name__ == "__main__":
    devices = sd.query_devices()
    print(devices)

    audio = recordAudio(5)

    newAudio = sd.playrec(audio, blocking=True)
    newNewAudio = sd.playrec(newAudio, blocking=True)

    playAudio(audio)
    playAudio(newAudio)
    playAudio(newNewAudio)