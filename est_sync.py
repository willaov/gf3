from cmath import exp
from ctypes import util
from random import sample
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import signal as sig
import wave
from scipy.io import wavfile
from standards import *

sns.set()

def countData(intput_audio):
    expected_length = (FFT_LEN+PREFIX_LEN)*FRAME_LENGTH + (FFT_LEN*KNOWN_SYMBOLS+PREFIX_LEN)*2 + (SAMPLING_FREQ*CHIRP_LENGTH)*2
    print("lenin", len(intput_audio))
    print("ex", expected_length)
    return int(np.ceil(len(intput_audio) / (expected_length + SAMPLING_FREQ*(CHIRP_LENGTH*2 + PRE_TRANSMISSION_SILENCE*2)))*2+2)

def findStart(OFDM_signal, input_signal, plot=False, count=None):
    a_full = sig.convolve(OFDM_signal, input_signal[::-1])
    if count is None:
        count = countData(OFDM_signal)
    while True:
        a = a_full.copy()
        if plot:
            plt.plot(a)
        l = []
        for i in range(count):
            if plot:
                plt.axvline(np.argmax(a),color = 'red', linestyle = 'dashed')
            l.append(np.argmax(a))
            a[np.argmax(a)-1000:np.argmax(a)+1000] = [0]*2000
        l = np.sort(l)
        diffs = np.diff(l)
        print(l)
        print(diffs)
        if plot:
            plt.title(f"{len(l)/2 - 1} Frames")
            plt.show()
        if np.min(diffs) > SAMPLING_FREQ*CHIRP_LENGTH*0.9:
            break
        else:
            count -= 2
    return l, a

class EstSync:
    def __init__(self, input_audio, output_audio, signal_length) -> None:
        self.input_audio = input_audio
        self.output_audio = output_audio
        self.signal_length = signal_length
    
    def synchronize(self, method='convolution', plot=False):
        if method == 'convolution':
            match = np.convolve(self.output_audio, self.input_audio[::-1])

            self.signal_end = np.argmax(match)
            # TODO: Bodged fix, should be replaced/checked
            if True:   # Only needed for double chirp
                saveMax = match[self.signal_end]
                match[self.signal_end] = 0
                otherEnd = np.argmax(match)
                match[self.signal_end] = saveMax
                if otherEnd > self.signal_end:
                    self.signal_end = otherEnd
            self.signal_start = self.signal_end - self.signal_length

            print(f"Signal ended at: {self.signal_end}")
            print(f"Signal started at {self.signal_start}")

            self.sync_input = self.input_audio[self.signal_start:self.signal_end]
            self.sync_output = self.output_audio[self.signal_start:self.signal_end]

            if plot:
                plt.plot(match)
                plt.show()

            return match
        elif method == 'conv_all':
            l, a = findStart(self.output_audio, self.input_audio, plot=plot)
            self.signal_start = l[0] - self.signal_length
            self.signal_end = l[-1]
            self.frames = []
            l = l[1:-1]
            for i in range(0, len(l), 2):
                self.frames.append(l[i:i+2])
            for i in range(0, len(self.frames)):
                self.frames[i][1] -= self.signal_length
            print(f"{len(self.frames)} Frames", self.frames)
        else:
            raise "Method not defined"

    def est_channel_freq(self, input_signal, output_signal, plot=False):
        input_f = np.fft.fft(input_signal)
        output_f = np.fft.fft(output_signal)

        channel_r = output_f/input_f

        if plot:
            plt.plot(abs(channel_r))

        return channel_r

    def channel_freq_acc(self, est_channel_f, true_channel_f, plot=False):
        print(f"MSE = {np.mean((true_channel_f - est_channel_f)**2)}")
        print(f"Variance = {np.var(true_channel_f - est_channel_f)**2}")

        if plot:
            plt.plot(abs(true_channel_f) - abs(est_channel_f))
