import numpy as np
from will_fns import decodeKnownChannel


if __name__ == "__main__":
    channel = np.genfromtxt('data/channel.csv', delimiter=',')
    file = np.genfromtxt('data/file5.csv', delimiter=',')
    decodeKnownChannel(file, channel)
