import numpy as np


def gray(n):
    ret = n ^ (n >> 1)
    return ret


def getArgand(zs, vals=None, lines=5, height=1.5, size=2):
    try:
        str_out = ""
        zlocs = []
        spacing = height*2.0/float(lines)
        halfway = int(lines/2)
        for z in zs:
            zloc = (
                int(round(np.real(z)/spacing)) + halfway,
                int(round(np.imag(z)/spacing)) + halfway
            )
            zlocs.append(zloc)
        out = []
        for i in range(lines):
            out.append([])
        for row in out:
            for i in range(lines):
                row.append(" "*size)
        for i in range(len(zlocs)):
            loc = zlocs[i]
            val = vals[i] if vals is not None else i
            out[loc[1]][loc[0]] = f"{val:*^{size}x}"
        out.reverse()

        str_out += "+" + "-"*(lines*size) + "+\n"
        for row in out:
            str_out += "|"
            for val in row:
                str_out += val
            str_out += '|\n'
        str_out += "+" + "-"*(lines*size) + "+"
        return str_out
    except IndexError:
        height += 1.0
        return getArgand(zs, vals, lines, height, size)


class Constellation:
    def __init__(self, n, cw=False):
        assert np.log2(n) == round(np.log2(n))
        self.encoding = {}
        self.dir = -1 if cw else 1
        self.n = n
        self.codes = []
        for i in range(n):
            theta = np.pi/4 + self.dir*2*np.pi*i/self.n
            self.encoding[gray(i)] = np.exp(1j*theta)
            self.codes.append(gray(i))

    def decode(self, z):
        closest = None
        min_dist = np.inf
        for (k, z_hat) in self.encoding.items():
            if np.abs(z - z_hat) < min_dist:
                min_dist = np.abs(z - z_hat)
                closest = k
        return closest

    def encode(self, b):
        return self.encoding[b]
    
    def getLLRs(self, z):
        zero = self.encode(0)
        dist = np.real(zero)
        bit2Mag = np.real(z)
        bit1Mag = np.imag(z)
        return bit1Mag, bit2Mag

    def __repr__(self):
        return f"{self.n}PSK\n" +\
            getArgand(self.encoding.values(), vals=self.codes, lines=int(2*np.log2(self.n) + 1))


def getBytes(data):
    data_bytes = []
    leftover = []
    while True:
        byte = 0
        numInByte = 0
        exit = False
        for i in range(4):
            byte = byte << 2
            try:
                thing = data.pop(0)
                numInByte += 1
            except IndexError:
                byte = byte >> 2
                for _ in range(numInByte):
                    leftover.append(byte & 0b11)
                    byte = byte >> 2
                leftover.reverse()
                # print("Ran out", leftover, numInByte)
                exit = True
                break
            byte |= thing
        if exit:
            break
        data_bytes.append(byte)
    return data_bytes, leftover


def decodeKnownChannel(file, channel):
    qpsk = Constellation(4, cw=False)
    # FFT of channel for later
    channel_f = np.fft.fft(channel, 1024)
    # Remove prefix and extract first block
    file = file[32:]
    block1 = file[:1024]
    file = file[1024:]
    # FFT and remove channel
    symbols = np.fft.fft(block1, 1024)
    symbols /= channel_f
    # Decode from symbols
    decoded = []
    for symbol in symbols:
        decoded.append(qpsk.decode(symbol))
    # Data in bins 1-511
    data = decoded[1:512]
    null_count = 0
    filename = ""
    filesize = ""
    removed = 0
    # Get the file name
    # File format is {name in ascii}\0{size is ascii}\0{file data}
    while True:
        # Take next byte of data
        byte_data = [data.pop(0), data.pop(0), data.pop(0), data.pop(0)]
        byte = getBytes(byte_data)[0][0]
        removed += 1
        char = bytes([byte]).decode('ascii')
        if char == '\0':
            # Found NULL
            null_count += 1
            if null_count > 1:
                break
            else:
                continue
        if null_count == 0:
            # File name
            filename += char
        elif null_count == 1:
            # File size
            filesize += char
    filesize = int(filesize)
    print(f"Now decoding {filename} of length {filesize}")

    num_blocks = 1
    # All data to be appended with new data to get all the 2bit chunks in one list
    all_data = data
    print(len(data))
    while len(all_data) < filesize*4:
        num_blocks += 1
        # Remove prefix and extract next block
        file = file[32:]
        block = file[:1024]
        file = file[1024:]
        # FFT and remove channel
        symbols = np.fft.fft(block, 1024)
        symbols /= channel_f
        # Decode from symbols
        decoded = []
        for symbol in symbols:
            decoded.append(qpsk.decode(symbol))
        data = decoded[1:512]
        all_data.extend(data.copy())
        print(f"Decoded {int(len(all_data)/4)} bytes out of {filesize}  ", end="\r")
    print("\nDone")

    # Decode the 2bit pairs to bytes
    all_data_bytes, all_data_left = getBytes(all_data.copy())

    # Trim data
    data_bytes = all_data_bytes[:filesize]
    # data_bytes = np.trim_zeros(all_data_bytes, 'b')
    print(len(data_bytes), filesize, "should be equal")
    # Assertions and info for WAV files, left in for reference
    # assert data_bytes[8:12] == [0x57, 0x41, 0x56, 0x45]
    # assert data_bytes[12:16] == [0x66, 0x6d, 0x74, 0x20]
    # print("Size", f'{[f"{val:02x}" for val in data_bytes[40:44]]}', f"{filesize-44:08x}")

    f = open(f"{filename}", 'wb')
    f.write(bytes(data_bytes))
    f.close()
