import lorem
import numpy as np


def genTextFile(name, numLines=None, filesize=None):
    lines = []
    if numLines is not None:
        for i in range(numLines):
            lines.append(f"{lorem.sentence()}\n")
    elif filesize is not None:
        size = 0
        while size < filesize:
            lines.append(f"{lorem.sentence()}\n")
            size = len("".join(lines))
            print(f"Size {size}     ", end='\r')
        print("Done")
    print(lines)
    f = open(name, 'w')
    f.writelines(lines)
    f.close()


if __name__ == "__main__":
    genTextFile("files/lorem.txt", filesize=20_000)
